package com.gitee.sop.bookweb.param;

import lombok.Data;

/**
 * @author tanghc
 */
@Data
public class BookParam {
    private int id;
}
